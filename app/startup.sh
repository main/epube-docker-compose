#!/bin/sh -e

if ! id app >/dev/null 2>&1; then
	# what if i actually need a duplicate GID/UID group?

	addgroup -g $OWNER_GID app || echo app:x:$OWNER_GID:app | \
		tee -a /etc/group

	adduser -D -h /var/www/html -G app -u $OWNER_UID app || \
		echo app:x:$OWNER_UID:$OWNER_GID:Linux User,,,:/var/www/html:/bin/ash | tee -a /etc/passwd
fi

DST_DIR=/var/www/html/books
SRC_REPO=https://git.tt-rss.org/fox/the-epube.git

[ -e $DST_DIR ] && rm -f $DST_DIR/.app_is_ready

export PGPASSWORD=$DB_PASS

[ ! -e /var/www/html/index.php ] && cp ${SCRIPT_ROOT}/index.php /var/www/html

if [ ! -d $DST_DIR/.git ]; then
	mkdir -p $DST_DIR
	echo cloning epube source from $SRC_REPO to $DST_DIR...
	git clone $SRC_REPO $DST_DIR || echo error: failed to clone master repository.
else
	echo updating epube source in $DST_DIR from $SRC_REPO...
	cd $DST_DIR && \
		git config core.filemode false && \
		git config pull.rebase false && \
		git pull origin master || echo error: unable to update master repository.
fi

if [ ! -e $DST_DIR/index.php ]; then
	echo "error: epube index.php missing (git clone failed?), unable to continue."
	exit 1
fi

if [ -r ${SCRIPT_ROOT}/restore.db ]; then
	cp ${SCRIPT_ROOT}/restore.db ${DST_DIR}/${EPUBE_SCRATCH_DB}
fi

chown -R $OWNER_UID:$OWNER_GID $DST_DIR \
	/var/log/php8

for d in db sessions; do
	chmod -R 777 $DST_DIR/$d
done

cp ${SCRIPT_ROOT}/config.docker.php $DST_DIR/config.php

if [ ! -z "${EPUBE_XDEBUG_ENABLED}" ]; then
	if [ -z "${EPUBE_XDEBUG_HOST}" ]; then
		export EPUBE_XDEBUG_HOST=$(ip ro sh 0/0 | cut -d " " -f 3)
	fi
	echo enabling xdebug with the following parameters:
	env | grep EPUBE_XDEBUG
	cat > /etc/php8/conf.d/50_xdebug.ini <<EOF
zend_extension=xdebug.so
xdebug.mode=develop,trace,debug
xdebug.start_with_request = yes
xdebug.client_port = ${EPUBE_XDEBUG_PORT}
xdebug.client_host = ${EPUBE_XDEBUG_HOST}
EOF
fi

sed -i.bak "s/^\(memory_limit\) = \(.*\)/\1 = ${PHP_WORKER_MEMORY_LIMIT}/" \
	/etc/php8/php.ini

sed -i.bak "s/^\(pm.max_children\) = \(.*\)/\1 = ${PHP_WORKER_MAX_CHILDREN}/" \
	/etc/php8/php-fpm.d/www.conf

sudo -Eu app php8 $DST_DIR/update.php --update-schema=force-yes

rm -f /tmp/error.log && mkfifo /tmp/error.log && chown app:app /tmp/error.log

(tail -q -f /tmp/error.log >> /proc/1/fd/2) &

if ! sudo -Eu app php8 $DST_DIR/update.php --user-list | grep -q "$EPUBE_ADMIN_USER"; then
	sudo -Eu app php8 $DST_DIR/update.php --user-add "$EPUBE_ADMIN_USER:$EPUBE_ADMIN_PASS"
fi

touch $DST_DIR/.app_is_ready

exec /usr/sbin/php-fpm8 --nodaemonize --force-stderr -R
